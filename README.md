# Run Length Application

##What does this project do?
This project is a Spring Boot app that encodes and decodes text. Encoding text involves detecting
characters consecutively repeated more than five times in an input and outputting them in a special 
format, such that an input like:

    aaaaabbbbbb!!ddddddd

would be output as:

    aaaaa{b;6}!!{d;7}

Decoding has the opposite effect such that an input like:

    c{o;10}l!
    
would be output as:

    cooooooooool!

##Usage Instructions: Encoding
To encode a string, start the Spring Boot app and send a POST request to 
localhost:8080/runLength/encode
along with a JSON body in the following format:

    {
      "text" : "text-you-want-to-encode"
    }
    
##Usage Instructions: Decoding
To decode a string, start the Spring boot app and send a POST request to localhost:8080/
runLength/decode
along with a JSON body in the following format:

    {
      "text" : "text-you-want-to-encode"
    }
    
##Response Format
A response from the application for both encoding and decoding looks like this:

    {
      "text": "{a;7}",
      "time": 3671
    }
    
where text is the encoded or decoded string, and time is the number of seconds the request took 
to execute.

# Disclaimers
- This repo has been created by pushing commits directly to master as I go along, this obviously 
would not happen under normal circumstances however due to the nature of the project I felt this 
would be more convenient.
- Several classes in this project make use of logging. I did consider using AOP to insert LOG 
statements at various points however I did not want to over-engineer the project, in addition 
to the fact that the LOG statements inserted do help with understanding the code a little more.
- The decode function has been coded under the assumption that an input has been properly encoded
i.e. if a user sent in a decode request with input {a;3}, then this was obviously not decoded by
the decoder in this project. Under normal circumstances I would've clarified this requirement with
the product owner and taken necessary actions.
- I have deliberately not included any exception handling in this project so as to not over-engineer
it as there is not much within the source code that could go wrong. Again, under working circumstances
I would've clarified this with the product owner and taken action by catching the right kind of
exceptions and returning an appropriate HTTP response code and body for each kind of exception.