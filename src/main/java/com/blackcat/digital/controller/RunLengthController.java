package com.blackcat.digital.controller;

import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import com.blackcat.digital.service.DecoderService;
import com.blackcat.digital.service.EncoderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller contains two endpoints for encoding and decoding. Calls service using wrapper request
 * object.
 */
@RestController
@RequestMapping("/runLength")
public class RunLengthController {

  private static final Logger LOG = LoggerFactory.getLogger(RunLengthController.class);

  private final EncoderService encoderService;
  private final DecoderService decoderService;

  public RunLengthController(final EncoderService encoderService,
      final DecoderService decoderService) {
    this.encoderService = encoderService;
    this.decoderService = decoderService;
  }

  @PostMapping("/encode")
  public RunLengthResponse encode(@RequestBody final RunLengthRequest request) {
    LOG.info("Calling encoder service with string: {}", request.getText());
    return encoderService.encode(request);
  }

  @PostMapping("/decode")
  public RunLengthResponse decode(@RequestBody final RunLengthRequest request) {
    LOG.info("Calling decoder service with string: {}", request.getText());
    return decoderService.decode(request);
  }


}
