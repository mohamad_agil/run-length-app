package com.blackcat.digital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot application to run the app as a service.
 */

@SpringBootApplication
public class RunLengthApplication {

  public static void main(String[] args) {
    SpringApplication.run(RunLengthApplication.class);
  }
}
