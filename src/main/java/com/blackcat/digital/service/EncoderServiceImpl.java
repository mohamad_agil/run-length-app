package com.blackcat.digital.service;

import com.blackcat.digital.domain.Encoder;
import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Helper class for encoder
 */

@Service
public class EncoderServiceImpl implements EncoderService {

  private static final Logger LOG = LoggerFactory.getLogger(DecoderServiceImpl.class);

  private final Encoder encoder;

  public EncoderServiceImpl(Encoder encoder) {
    this.encoder = encoder;
  }

  public RunLengthResponse encode(RunLengthRequest request) {
    long startTime = System.nanoTime();
    LOG.info("Starting encoding process, start time: {} nanoseconds.", startTime);
    String encodedText = encoder.encode(request.getText());
    return new RunLengthResponse(encodedText, System.nanoTime() - startTime);
  }
}
