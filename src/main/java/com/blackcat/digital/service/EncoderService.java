package com.blackcat.digital.service;

import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;

public interface EncoderService {

  RunLengthResponse encode(RunLengthRequest request);

}
