package com.blackcat.digital.service;

import com.blackcat.digital.domain.Decoder;
import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Helper class for decoder
 */

@Service
public class DecoderServiceImpl implements DecoderService {

  private static final Logger LOG = LoggerFactory.getLogger(DecoderServiceImpl.class);

  private final Decoder decoder;

  public DecoderServiceImpl(Decoder decoder) {
    this.decoder = decoder;
  }

  public RunLengthResponse decode(RunLengthRequest request) {
    long startTime = System.nanoTime();
    LOG.info("Starting decoding process, start time: {} nanoseconds.", startTime);
    String decodedText = decoder.decode(request.getText());
    return new RunLengthResponse(decodedText, System.nanoTime() - startTime);
  }
}
