package com.blackcat.digital.service;

import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;

public interface DecoderService {

  RunLengthResponse decode(RunLengthRequest request);

}
