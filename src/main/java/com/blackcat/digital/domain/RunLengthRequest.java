package com.blackcat.digital.domain;

/**
 * Wrapper object for input to decoder or encoder
 */
public class RunLengthRequest {

  private String text;

  public RunLengthRequest() {
  }

  public RunLengthRequest(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
