package com.blackcat.digital.domain;


public interface Decoder {

  String decode(String inputStr);

}
