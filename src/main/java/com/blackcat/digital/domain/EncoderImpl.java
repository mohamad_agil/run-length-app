package com.blackcat.digital.domain;

import static java.lang.String.format;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Encodes a string such that any character that is repeated consecutively more than 5 times is
 * represented in the format {a;6} e.g. bbbaaaaaa will be converted to bbb{a;6}
 */

@Component
public class EncoderImpl implements Encoder {

  private static final Logger LOG = LoggerFactory.getLogger(EncoderImpl.class);

  public String encode(final String inputStr) {
    StringBuilder outputStringBuilder = new StringBuilder();

    for (int i = 0; i < inputStr.length(); i++) {
      int noOfConsecutiveRepetitions = 1;

      while (indexIsNotAtTheEndOfTheInput(inputStr, i, noOfConsecutiveRepetitions)
          && currentCharIsEqualToNextChar(inputStr, i, noOfConsecutiveRepetitions)) {
        noOfConsecutiveRepetitions++;
      }

      if (noOfConsecutiveRepetitions > 5) {
        final String encodedToken = format("{%s;%s}", inputStr.charAt(i), noOfConsecutiveRepetitions);
        LOG.info("Character '{}' repeated {} times, encoding to '{}' and appending to output.",
            inputStr.charAt(i), noOfConsecutiveRepetitions, encodedToken);
        outputStringBuilder.append(encodedToken);
        i = calculateIndexOfCharacterAfterRepeat(i, noOfConsecutiveRepetitions);

      } else {
        LOG.info("Appending character '{}' to output.", inputStr.charAt(i));
        outputStringBuilder.append(inputStr.charAt(i));
      }
    }
    return outputStringBuilder.toString();
  }

  private int calculateIndexOfCharacterAfterRepeat(final int i,
      final int noOfConsecutiveRepetitions) {
    return i + noOfConsecutiveRepetitions - 1;
  }

  private boolean indexIsNotAtTheEndOfTheInput(String inputStr, int i,
      int noOfConsecutiveRepetitions) {
    return i + noOfConsecutiveRepetitions < inputStr.length();
  }

  private boolean currentCharIsEqualToNextChar(String inputStr, int i,
      int noOfConsecutiveRepetitions) {
    return inputStr.charAt(i) == inputStr.charAt(i + noOfConsecutiveRepetitions);
  }

}
