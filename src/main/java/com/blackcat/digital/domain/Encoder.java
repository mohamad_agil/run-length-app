package com.blackcat.digital.domain;

public interface Encoder {

  String encode(String s);
}
