package com.blackcat.digital.domain;

/**
 * Wrapper object for output from decoder or encoder. Time attribute represents the number of
 * nanoseconds the operation took to execute.
 */
public class RunLengthResponse {

  private final String text;
  private final long time;

  public RunLengthResponse(String text, long time) {
    this.text = text;
    this.time = time;
  }

  public String getText() {
    return text;
  }

  public long getTime() {
    return time;
  }
}
