package com.blackcat.digital.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Decodes an input string such that bbbb{a;6}bbb is converted into bbbbaaaaaabbb
 */

@Component
public class DecoderImpl implements Decoder {

  private static final Logger LOG = LoggerFactory.getLogger(DecoderImpl.class);
  private static final String ENCODED_TOKEN_REGEX = "\\{(.);(\\d+)\\}";
  private static final char ENCODED_TOKEN_START_CHARACTER = '{';

  public String decode(final String inputStr) {

    StringBuilder outputStringBuilder = new StringBuilder();
    Pattern pattern = Pattern.compile(ENCODED_TOKEN_REGEX);

    for (int i = 0; i < inputStr.length(); i++) {

      if (inputStr.charAt(i) == ENCODED_TOKEN_START_CHARACTER) {
        String stringRemainder = inputStr.substring(i, inputStr.length());
        Matcher matcher = pattern.matcher(stringRemainder);

        if (matcher.find()) {
          LOG.info("Found encoded token: {} at index {}.", matcher.group(), i);
          appendRepeatedCharacterNTimesToOutput(outputStringBuilder, matcher);
          final int lengthOfRunLengthPattern = matcher.group().length();
          i = calculateIndexOfCharacterAfterEncodedToken(i, lengthOfRunLengthPattern);
        }
      } else {
        LOG.info("Appending character {} to output.", inputStr.charAt(i));
        outputStringBuilder.append(inputStr.charAt(i));
      }
    }

    return outputStringBuilder.toString();
  }

  private void appendRepeatedCharacterNTimesToOutput(StringBuilder outStringBuilder, Matcher matcher) {
    final String repeatedChar = matcher.group(1);
    int numberOfRepetitions = Integer.parseInt(matcher.group(2));
    LOG.info("Appending character '{}' {} times to output.", repeatedChar, numberOfRepetitions);
    for (int j = 0; j < numberOfRepetitions; j++) {
      outStringBuilder.append(repeatedChar);
    }
  }

  private int calculateIndexOfCharacterAfterEncodedToken(final int i, final int lengthOfEncodedToken) {
    return i + (lengthOfEncodedToken - 1);
  }
}
