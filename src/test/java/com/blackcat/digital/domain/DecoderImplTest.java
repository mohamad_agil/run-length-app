package com.blackcat.digital.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class DecoderImplTest {

  private DecoderImpl decoder;
  private String input;
  private String expectedOutput;
  private String actualOutput;

  @Before
  public void setUp() {
    decoder = new DecoderImpl();
    input = "";
    expectedOutput = "";
    actualOutput = "";
  }

  @Test
  public void decodeEmptyString() {
    input = "";
    expectedOutput = "";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithLessThanFiveCharacters() {
    input = "aaa";
    expectedOutput = "aaa";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithOneEncoding() {
    input = "{a;6}";
    expectedOutput = "aaaaaa";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringStartingWithOneEncoding() {
    input = "{a;6}hello";
    expectedOutput = "aaaaaahello";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithOneEncodingInTheMiddle() {
    input = "hi{a;6}hello";
    expectedOutput = "hiaaaaaahello";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithOneEncodingAtTheEnd() {
    input = "hello{a;6}";
    expectedOutput = "helloaaaaaa";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithMixOfLettersAndEncodings() {
    input = "hello{a;6}hi{b;7}bonjour{c;10}";
    expectedOutput = "helloaaaaaahibbbbbbbbonjourcccccccccc";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithEncodingsOnly() {
    input = "{a;6}{b;7}{c;10}";
    expectedOutput = "aaaaaabbbbbbbcccccccccc";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void decodeStringWithCurlyBraces() {
    input = "{{;6}{{;10}{};11}";
    expectedOutput = "{{{{{{{{{{{{{{{{}}}}}}}}}}}";
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  private void whenDecodeIsInvoked() {
    actualOutput = decoder.decode(input);
  }

  private void thenTheOutputShouldBeAsExpected() {
    assertThat(actualOutput).isEqualTo(expectedOutput);
  }

}