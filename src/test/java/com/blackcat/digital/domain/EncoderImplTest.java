package com.blackcat.digital.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class EncoderImplTest {

  private EncoderImpl encoder;
  private String input;
  private String expectedOutput;
  private String actualOutput;

  @Before
  public void setUp() {
    encoder = new EncoderImpl();
    input = "";
    expectedOutput = "";
    actualOutput = "";
  }

  @Test
  public void encodeEmptyString() {
    input = "";
    expectedOutput = "";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithLessThanFiveConsecutiveLetters() {
    input = "aaa";
    expectedOutput = "aaa";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithFiveConsecutiveLetters() {
    input = "aaaaa";
    expectedOutput = "aaaaa";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithSixConsecutiveLetters() {
    input = "aaaaaa";
    expectedOutput = "{a;6}";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringStartingWithConsecutiveLetters() {
    input = "hhhhhhhhhhello";
    expectedOutput = "{h;10}ello";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithConsecutiveLettersInTheMiddle() {
    input = "hellllllloo";
    expectedOutput = "he{l;7}oo";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithConsecutiveLettersAtTheEnd() {
    input = "helloooooooooo";
    expectedOutput = "hell{o;10}";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithMultipleSetsOfConsecutiveLetters() {
    input = "heeeeelllllloooooo";
    expectedOutput = "heeeee{l;6}{o;6}";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringCompletelyMadeUpOfConsecutiveLetters() {
    input = "hhhhhheeeeeelllllloooooo";
    expectedOutput = "{h;6}{e;6}{l;6}{o;6}";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  @Test
  public void encodeStringWithConsecutiveCurlyBraces() {
    input = "{{{{{{}}}}}}";
    expectedOutput = "{{;6}{};6}";
    whenEncodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  private void whenEncodeIsInvoked() {
    actualOutput = encoder.encode(input);
  }

  private void thenTheOutputShouldBeAsExpected() {
    assertThat(actualOutput).isEqualTo(expectedOutput);
  }

}