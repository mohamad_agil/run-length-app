package com.blackcat.digital;


import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.blackcat.digital.controller.RunLengthController;
import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import com.blackcat.digital.service.DecoderService;
import com.blackcat.digital.service.EncoderService;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class RunLengthApplicationIntegrationTest {

  private String url;
  private RequestSpecification requestSpecification;
  private ResponseSpecification responseSpecification;
  private Response actualResponse;
  private RunLengthResponse expectedResponse = new RunLengthResponse("testResponse", 0);

  @LocalServerPort
  private int port = 8080;

  @Bean(name = "mockEncoderService")
  @Primary
  public EncoderService encoderService() {
    EncoderService encoderService = Mockito.mock(EncoderService.class);
    when(encoderService.encode(any(RunLengthRequest.class))).thenReturn(expectedResponse);
    return encoderService;
  }

  @Bean(name = "mockDecoderService")
  @Primary
  public DecoderService decoderService() {
    DecoderService decoderService = Mockito.mock(DecoderService.class);
    when(decoderService.decode(any(RunLengthRequest.class))).thenReturn(expectedResponse);
    return decoderService;
  }

  @Autowired
  private RunLengthController runLengthController;

  @Test
  public void shouldSuccessfullyEncode() throws JSONException {
    givenAUrl();
    andARunLengthRequest();
    andAResponseSpecification();
    whenEncodeIsInvoked();
    thenASuccessfulResponseShouldBeReturned();
  }

  @Test
  public void shouldSuccessfullyDecode() throws JSONException {
    givenAUrl();
    andARunLengthRequest();
    andAResponseSpecification();
    whenDecodeIsInvoked();
    thenASuccessfulResponseShouldBeReturned();
  }

  private void givenAUrl() {
    url = "http://localhost:" + port + "/runLength";
  }

  private void andARunLengthRequest() throws JSONException {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("text", expectedResponse.getText());
    requestSpecification = new RequestSpecBuilder()
        .addHeader("Content-Type", "application/json")
        .setBody(jsonObject.toString())
        .setBaseUri(url)
        .build();
  }

  private void andAResponseSpecification() {
    responseSpecification = new ResponseSpecBuilder()
        .expectStatusCode(HttpStatus.OK.value())
        .expectBody("text", equalTo(expectedResponse.getText()))
        .build();
  }


  private void whenEncodeIsInvoked() {
    actualResponse = given()
        .spec(requestSpecification)
        .when()
        .post("/encode");
  }

  private void whenDecodeIsInvoked() {
    actualResponse = given()
        .spec(requestSpecification)
        .when()
        .post("/decode");
  }


  private void thenASuccessfulResponseShouldBeReturned() {
    actualResponse.then()
        .spec(responseSpecification);
  }
}
