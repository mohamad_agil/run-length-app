package com.blackcat.digital.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.blackcat.digital.domain.Decoder;
import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DecoderServiceImplTest {

  private DecoderServiceImpl testClass;
  private RunLengthRequest runLengthRequest;
  private RunLengthResponse actualOutput;
  private String inputText = "input";
  private String expectedText = "response";

  @Mock
  private Decoder mockDecoder;

  @Test
  public void shouldReturnRunLengthResponse() {
    givenADecoderServiceImplWithAMockedDecoder();
    andARunLengthRequest();
    andTheMockDecoderIsConfiguredToReturnSuccess();
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  private void givenADecoderServiceImplWithAMockedDecoder() {
    testClass = new DecoderServiceImpl(mockDecoder);
  }

  private void andARunLengthRequest() {
    runLengthRequest = new RunLengthRequest(inputText);
  }

  private void andTheMockDecoderIsConfiguredToReturnSuccess() {
    when(mockDecoder.decode(runLengthRequest.getText())).thenReturn(expectedText);
  }

  private void whenDecodeIsInvoked() {
    actualOutput = testClass.decode(runLengthRequest);
  }

  private void thenTheOutputShouldBeAsExpected() {
    assertThat(actualOutput.getText()).isEqualTo(expectedText);
    verify(mockDecoder).decode(inputText);
    verifyNoMoreInteractions(mockDecoder);
  }

}