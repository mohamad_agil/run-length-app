package com.blackcat.digital.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.blackcat.digital.domain.Encoder;
import com.blackcat.digital.domain.RunLengthRequest;
import com.blackcat.digital.domain.RunLengthResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EncoderServiceImplTest {

  private EncoderServiceImpl testClass;
  private RunLengthRequest runLengthRequest;
  private RunLengthResponse actualOutput;
  private String inputText = "input";
  private String expectedText = "response";

  @Mock
  private Encoder mockEncoder;

  @Test
  public void shouldReturnRunLengthResponse() {
    givenAnEncoderServiceImplWithAMockedEncoder();
    andARunLengthRequest();
    andTheMockEncoderIsConfiguredToReturnSuccess();
    whenDecodeIsInvoked();
    thenTheOutputShouldBeAsExpected();
  }

  private void givenAnEncoderServiceImplWithAMockedEncoder() {
    testClass = new EncoderServiceImpl(mockEncoder);
  }

  private void andARunLengthRequest() {
    runLengthRequest = new RunLengthRequest(inputText);
  }

  private void andTheMockEncoderIsConfiguredToReturnSuccess() {
    when(mockEncoder.encode(runLengthRequest.getText())).thenReturn(expectedText);
  }

  private void whenDecodeIsInvoked() {
    actualOutput = testClass.encode(runLengthRequest);
  }

  private void thenTheOutputShouldBeAsExpected() {
    assertThat(actualOutput.getText()).isEqualTo(expectedText);
    verify(mockEncoder).encode(inputText);
    verifyNoMoreInteractions(mockEncoder);
  }

}